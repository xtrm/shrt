import type Site from "lume/core/site.ts";
import { Page } from "lume/core/file.ts";

const SHRT_INDEX_PATH = "../index/urls.txt";

export default function generateShrtPages() {
  return (site: Site) => {
    site.process("*", (pages: Page[], allPages: Page[]) => {
      const indexFile = Deno.readTextFileSync(SHRT_INDEX_PATH);

      indexFile.split("\n").forEach((urlMap) => {
        urlMap = urlMap.trim();
        if (!urlMap) return;
      
        // split only the first :
        let targetArray = urlMap.split(":");
        let shrtId = targetArray.shift();
        let target = targetArray.join(":");
        
        let page = Page.create({
          url: `/${shrtId}/index.html`,
          content: `<!DOCTYPE html><html><head><meta charset="utf-8"><link rel="stylesheet" href="style.css"><meta http-equiv="refresh" content="0;url=${target}"></head><body><h1>Redirecting you</h1><p>Redirecting you to: <a href="${target}">${target}</a></p><script>window.location="${target}";</script></body></html>`,
        });

        // Give the page a custom `src` object to show the target URL on the build output
        page.src = {
          entry: {
            flags: new Set(["remote"]),
            src: target,
          },
        };
        allPages.push(page);
      });
    });
  };
}

// vim: ts=2:sw=2:et
