import lume from "lume/mod.ts";
import lightningcss from "lume/plugins/lightningcss.ts";
import robots from "lume/plugins/robots.ts";
import generateShrtPages from "./generateShrtPages.ts";

const site = lume({
  location: new URL("https://shrt.xtrm.me/"),
});

site.use(lightningcss());
site.use(robots({
  allow: [],
  disallow: "*",
}));

site.use(generateShrtPages());

export default site;

// vim: ts=2 sw=2 et:
