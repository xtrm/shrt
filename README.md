# shrt

> a Git-based URL shortener.

## how to use

if you really wish to use this utter mess, here's what to do:

1) Fork the repo
2) Clone it
3) Change the config in `scripts/shrt` to point to the proper directory/repo path
4) Change the URL in `site/_config.ts`
5) Add the `scripts` dir to your `PATH`
6) Pray that i didn't forget a step here lol
7) ???

## license

this project is released under the [ISC License](./LICENSE).
